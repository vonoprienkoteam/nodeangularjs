var bcrypt = require('bcrypt');

module.exports = {
    'secret': 'super.super.secret.shhh',
    'localDatabase': 'mongodb://localhost:27017/shop'
};

module.exports.config = {
    getEnv: function () {
        return {
            mailgun: {
                key: 'key-cc5f4eb3ba23938ff5126ed828bb325f',
                domain: 'sandbox4cd7db4c8830475bb6ce9f24a83af592.mailgun.org',
                login: 'postmaster@sandbox4cd7db4c8830475bb6ce9f24a83af592.mailgun.org',
                password: '1q2w3e'
            }
        }
    }
};

/**
 * Encrypt password through "bcrypt" module
 * @param password {string} - password for saving into db
 * @returns {*} {string} - encrypted by "bcrypt"
 */
module.exports.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};