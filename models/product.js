var mongoose = require('mongoose'),
    User = require('./user'),
    Category = require('./category'),
    Schema = mongoose.Schema;

// create a comment schema
var commentSchema = new Schema({
  user: {type: Schema.Types.ObjectId, ref: 'User'},
  text: {type: String, required: true},
  createdAt: {type: Date, default: Date.now},
  updatedAt: {type: Date}
});

// create a support schema
var supportSchema = new Schema({
  user_id: { type: Schema.Types.ObjectId, ref: 'User' },
  question: {type: String, required: true},
  answers: [{
    answer: {type: String},
    isAdmin: {type: Boolean, default: false}
  }],
  archive: {type: Boolean, default: false},
  createdAt: {type: Date, default: Date.now},
  updatedAt: {type: Date}
});

// create a product schema
var productSchema = new Schema({
  name: {type: String, required: true},
  description: {type: String, required: true},
  price: {type: Number, required: true},
  images: [String],
  comments: [commentSchema],
  support: [supportSchema],
  isInStock: {type: Boolean, default: true},
  category: {type: Schema.Types.ObjectId, ref: 'Category'},
  createdAt: {type: Date, default: Date.now},
  updatedAt: {type: Date}
});

function timeStamp(next) {
  var now = new Date();
  this.updatedAt = now;
  if (!this.createdAt) {
    this.createdAt = now;
  }
  next();
}

productSchema.pre('save', timeStamp);
commentSchema.pre('save', timeStamp);
supportSchema.pre('save', timeStamp);

// add a text index to the name & description fields
productSchema.index({
  name: 'text',
  description: 'text'
});

module.exports = mongoose.model('Product', productSchema);

