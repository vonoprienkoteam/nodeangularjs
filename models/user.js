// grab the things we need
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    Order = require('./order'),
    bcrypt = require('bcrypt');

// create a schema
var userSchema = new Schema({
    name: { type: String, required: true, unique: true },
    lastName: String,
    firstName: String,
    gender: String,
    avatar: String,
    email: String,
    phone: String,
    password: { type: String , required: true},
    passwordReset: String,
    banned: { type: Boolean, default: false},
    admin: String,
    token: String,
    orders: [{ type: Schema.Types.ObjectId, ref: 'Order' }],
    wishlist: [{ type: String }],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date}
});

userSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(8, function(err, salt) {
        if (err) return next(err);

        // hash the password along with our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('User', userSchema);

