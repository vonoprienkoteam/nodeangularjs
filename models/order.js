var mongoose = require('mongoose'),
    User = require('./user'),
    Schema = mongoose.Schema;

var orderSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    guest: String,
    products: [{
        id: String,
        name: String,
        quantity: Number,
        price: Number,
        total: Number
    }],
    message: String,
    phone: String,
    address: String,
    paid: {type: Boolean, default: false},
    totalCost: Number,
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});


function timeStamp(next){
    var now = new Date();
    this.updatedAt = now;
    if ( !this.createdAt ) {
        this.createdAt = now;
    }
    next();
}

orderSchema.pre('save', timeStamp);

module.exports = mongoose.model('Order', orderSchema);
