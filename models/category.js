var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var categorySchema = new Schema({
  name: { type: String, required: true, unique: true},
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});


function timeStamp(next){
  var now = new Date();
  this.updatedAt = now;
  if ( !this.createdAt ) {
    this.createdAt = now;
  }
  next();
}

categorySchema.pre('save', timeStamp);

module.exports = mongoose.model('Category', categorySchema);

