(function () {
    angular
        .module('myApp')
        .service('getData', getData);

    getData.$inject = ['$resource'];

    function getData($resource) {
        this.sendQuery = function(url) {
            return $resource(url);
        }
    }

})();
