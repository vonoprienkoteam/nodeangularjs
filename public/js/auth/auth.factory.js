myApp.factory('AuthenticationFactory', function($window) {
    return {
        isLogged: false,
        isAdmin: false,
        userId: "",
        check: function() {
            if ($window.sessionStorage.token && $window.sessionStorage.user) {
                this.phone = $window.sessionStorage.phone;
                this.isLogged = true;
                this.isAdmin = ($window.sessionStorage.admin === 'true');
                this.userId = $window.sessionStorage.userId;
            } else {
                this.isLogged = false;
                delete this.user;
            }
        }
    };
});
myApp.factory('UserAuthFactory', function($window, $location, $http, AuthenticationFactory, config) {
    return {
        login: function(name, password) {
            return $http.post(config.apiUrl + '/login', {
                name: name,
                password: password
            });
        },
        register: function(name, password, confirmpassword, usermail) {
            return $http.post(config.apiUrl + '/register', {
                name: name,
                password: password,
                confirmpassword: confirmpassword,
                usermail: usermail
            });
        },
        logout: function() {
            if (AuthenticationFactory.isLogged) {
                AuthenticationFactory.isLogged = false;
                delete AuthenticationFactory.user;
                delete AuthenticationFactory.admin;
                delete AuthenticationFactory.userId;
                delete $window.sessionStorage.token;
                delete $window.sessionStorage.user;
                delete $window.sessionStorage.admin;
                delete $window.sessionStorage.userId;
                delete $window.sessionStorage.phone;
                $location.path("#/");
            }
        }
    }
});
myApp.factory('TokenInterceptor', function($q, $window) {
    return {
        request: function(config) {
            config.headers = config.headers || {};
            if ($window.sessionStorage.token) {
                config.headers['X-Access-Token'] = $window.sessionStorage.token;
                config.headers['X-Key'] = $window.sessionStorage.user;
                config.headers['Content-Type'] = "application/json";
            }
            return config || $q.when(config);
        },
        response: function(response) {
            return response || $q.when(response);
        }
    };
});
myApp.factory('RestoreFactory', function($http, config) {
    var urlBase = config.apiUrl + '/user';
    var _restoreFactory = {};

    _restoreFactory.restore = function(email) {
        var data = JSON.stringify({
            email: email
        });
        return $http.post(urlBase + '/restore/forgot', data);
    };

    _restoreFactory.restoreReset = function(code, password) {
        var data = JSON.stringify({
            code: code,
            pass: password
        });
        return $http.post(urlBase + '/restore/reset', data);
    };

    _restoreFactory.Checkmail = function (email) {
        var data = JSON.stringify({
           email: email
        });
        return $http.post(config.apiUrl + '/checkmail', data);
    };

    return _restoreFactory;
});