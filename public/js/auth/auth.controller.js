myApp.controller('LoginCtrl', ['$scope', '$window', '$location', 'UserAuthFactory', 'AuthenticationFactory','RestoreFactory',
    function($scope, $window, $location, UserAuthFactory, AuthenticationFactory, RestoreFactory) {

        var myModalReset = $('#myModalReset');
        delete $scope.successReset;

            $scope.login = function() {
            var name = $scope.user.name,
                password = $scope.user.password;
            if (name !== undefined && password !== undefined) {
                UserAuthFactory.login(name, password).success(function(data) {
                    AuthenticationFactory.isLogged = true;
                    AuthenticationFactory.user = data.user.name;
                    AuthenticationFactory.admin = data.user.admin;
                    AuthenticationFactory.userId = data.user._id;
                    AuthenticationFactory.phone = data.user.phone;
                    $window.sessionStorage.token = data.token;
                    $window.sessionStorage.user = data.user.name; // to fetch the user details on refresh
                    $window.sessionStorage.admin = data.user.admin; // to fetch the user details on refresh
                    $window.sessionStorage.userId = data.user._id; // to fetch the user details on refresh
                    $window.sessionStorage.phone = data.user.phone; // to fetch the user details on refresh
                    $location.path("/");
                }).error(function(status) {
                    alert('Oops something went wrong!');
                });
            } else {
                alert('Invalid credentials');
            }
        };

        $scope.registration = function() {

            var name = $scope.user.name,
                password = $scope.user.password,
                confirmPassword = $scope.user.confirmpassword,
                userMail = $scope.user.email;

            if (name !== undefined && password !== undefined && password === confirmPassword) {
            if ($scope.user.email) {

                RestoreFactory.Checkmail($scope.user.email)
                    .then(function (response) {
                        if (response.data.email != $scope.user.email) {
                            UserAuthFactory.register(name, password, confirmPassword, userMail).success(function(data) {
                                AuthenticationFactory.isLogged = true;
                                AuthenticationFactory.user = data.user.name;
                                AuthenticationFactory.admin = false;
                                AuthenticationFactory.userId = data.user._id;
                                AuthenticationFactory.phone = data.user.phone;
                                $window.sessionStorage.token = data.token;
                                $window.sessionStorage.user = data.user.name; // to fetch the user details on refresh
                                $window.sessionStorage.admin = data.user.admin; // to fetch the user details on refresh
                                $window.sessionStorage.userId = data.user._id; // to fetch the user details on refresh
                                $window.sessionStorage.phone = data.user.phone; // to fetch the user details on refresh
                                $location.path("#/");
                            }).error(function(status) {
                                if (password != confirmPassword) {
                                    alert('Passwords are not equal!');
                                } else {
                                    alert(JSON.stringify(status));
                                    $scope.errmessage = status.message;
                                }
                            });
                        } else {
                            alert('This mailbox is not avaliable.');
                        }
                    }, function (status) {
                        console.log(status);
                    });
            }
            } else {
                alert('Invalid credentials');
            }
        };

        $scope.restoreForgot = function() {
            if ($scope.user.email) {
                RestoreFactory.Checkmail($scope.user.email)
                    .then(function (response) {
                        console.log('==========response');
                        console.log(response);
                        if (response.data !== "")
                            {
                                RestoreFactory.restore($scope.user.email)
                                    .then(function (response) {
                                        console.log(response);
                                    }, function (status) {
                                        console.log(status);
                                    });

                                myModalReset.on('shown.bs.modal', function () {
                                    $('#myInput').focus()
                                });

                                $('#myModal').modal('hide');
                                myModalReset.modal('show');

                            } else {
                                $('#myModal').modal('hide');
                                alert('This mailbox is not found in the database.');
                            }
                    }, function (status) {
                        console.log(status);
                    });
            }
        };

        $scope.restoreReset = function() {
            if ($scope.user.code && $scope.user.passwordreset === $scope.user.confirmPassword) {

                RestoreFactory.restoreReset($scope.user.code, $scope.user.passwordreset)
                    .then(function (response) {
                        console.log(response);
                        if (response.status == 200) {
                            $scope.successReset = "You success reset password.";
                        }
                    }, function (status) {
                        console.log(status);
                    });
            }
            myModalReset.modal('hide');
            $('#codereset').val("");
            $('#passwordreset').val("");
            $('#confirmreset').val("");
        };

        $('#myModal').on('shown.bs.modal', function () {
            $('#myInput').focus()
        })
    }
]);