myApp.controller("HomeCtrl", ['$scope', 'productsFactory', 'productService',
  function($scope, productsFactory, productService) {
    $scope.name = "Home Controller";
    $scope.products = [];
    $scope.totalProducts = 0;
    $scope.productsPerPage = 24;
    $scope.categories = [];
    $scope.currentCategory = "";

    $scope.pagination = {
        current: 1
    };

    // Access the factory and get products list
    $scope.getAll = function (page, category) {
      $scope.currentCategory = category;
      productsFactory.getProducts(page, category).then(function(data) {
        $scope.products = data.data.products;
        $scope.totalProducts = data.data.count;
        if (!page) $scope.pagination.current = 1;
      });
    };

    $scope.getAll();

    $scope.pageChanged = function (newPageNumber, category) {
      $scope.getAll(newPageNumber, category);
    };

    productsFactory.getCategories().then(function(data) {
      $scope.categories = data.data.categories;
      $scope.total_products = data.data.total;
    });

  if (sessionStorage.getItem("reviewed") !== null) {
        $scope.arr = sessionStorage.getItem("reviewed").split(',');
  }

      var reviewed = function() {
          productService.getReviewed($scope.arr)
              .then(function (response) {
                  var swiper = new Swiper('.swiper-container', {
                      slidesPerView: 5,
                      paginationClickable: true,
                      nextButton: '.swiper-button-next',
                      prevButton: '.swiper-button-prev',
                      spaceBetween: 30
                  });
                  $scope.reViewed = response.data.products;

              }, function (status) {
                  console.log(status);
              });
      };
      if ($scope.arr) {
          reviewed();
      }

  }
]);

myApp.controller("HeaderCtrl", ['$scope', '$location', 'UserAuthFactory', '$http', '$q', 'config',
    function ($scope, $location, UserAuthFactory, $http, $q, config) {
        $scope.isActive = function (route) {
            return route === $location.path();
        };
        $scope.logout = function () {
            UserAuthFactory.logout();
        };

        $scope.userSearch = "";

        //$scope.userSearchCallback = dataFactory(userSearch);

        $scope.userSearchCallback = function (params) {
            var defer = $q.defer();

            $http.get(config.apiUrl + "/products/search?q=" + params.query)
                .success(function (response) {

                    if (response.success) {
                        defer.resolve(response.products);
                    } else {
                        defer.resolve(response.message.split());
                    }
                });

            return defer.promise;
        };
    }
]);

myApp.controller("SetupCtrl", ['$scope', '$http', '$location', 'config',
    function ($scope, $http, $location, config) {
        $scope.createAdmin = function () {
            $http.get(config.apiUrl + '/setup').then(function (response) {
                alert(response.data.message);
                $location.path("/");
            }, function (status) {
                alert("Something went wrong :)");
            });
        }
    }
]);

