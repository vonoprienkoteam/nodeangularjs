myApp.factory('UserQuestionsFactory', function($http, config) {
    /** https://docs.angularjs.org/guide/providers **/
    var urlBase = config.apiUrl + '/product';
    var _questionsFactory = {};

    _questionsFactory.addQuestion = function(productId, question) {
        var data = JSON.stringify({
            question: question,
            productId: productId
        });
        return $http.post(urlBase + '/' + productId +'/question', data);
    };

    _questionsFactory.getQuestion = function(productId, questionId) {
        var data = JSON.stringify({
            productId: productId,
            questionId: questionId
        });
        return $http.get(config.apiUrl +'/user/products/' + productId + '/question/' + questionId, data);
    };

    _questionsFactory.addAnswer = function(productId, newAnswer, idQuestion) {
        var data = JSON.stringify({
            productId: productId,
            newAnswer: newAnswer,
            isAdmin: false,
            idQuestion: idQuestion
        });
        return $http.post(config.apiUrl +'/user/products/' + productId + '/answer', data);
    };

    _questionsFactory.getUserQuestions = function(userId) {
        var data = {
            userId: userId
        };
        return $http.get(config.apiUrl + '/user/' + userId + '/questions', data);
    };

    return _questionsFactory;
});