myApp.controller('UserQuestionsCtrl',
    ['$scope', 'AuthenticationFactory', 'UserQuestionsFactory', '$routeParams',

        function ($scope, AuthenticationFactory, UserQuestionsFactory, $routeParams) {

            var id = $routeParams.id,
                idProduct = $routeParams.idProduct,
                idQuestion = $routeParams.idQuestion;

            $scope.isLogged = AuthenticationFactory.isLogged;
            $scope.isAdmin = AuthenticationFactory.isAdmin;
            $scope.userId = AuthenticationFactory.userId;

            if ($scope.userId) {
                UserQuestionsFactory.getUserQuestions($scope.userId).then(function (data) {
                    $scope.products = data.data.products;
                });
            }




            if (idProduct && idQuestion) {
                UserQuestionsFactory.getQuestion(idProduct, idQuestion).then(function (data) {
                    $scope.support = data.data.product.filter(function(obj) {
                        if (obj._id === idQuestion) {
                            return obj;
                        }
                    });
                });
            }

            $scope.sendQuestion = function () {
                if ($scope.newQuestion) {
                    UserQuestionsFactory.addQuestion(id, $scope.newQuestion)
                        .then(function (response) {
                            $scope.questionLabel = "Thank you for your question.";
                        }, function (status) {
                            //console.log(status);
                        });

                    $scope.newQuestion = "";
                }
            };

            $scope.createUserAnswer = function () {
                if ($scope.newUserAnswer) {
                    UserQuestionsFactory.addAnswer(idProduct, $scope.newUserAnswer, idQuestion)
                        .then(function (response) {
                            $scope.questionLabel = "Thank you for message.";
                            getQuestion(idProduct, idQuestion);
                        }, function (status) {
                            //console.log(status);
                        });
                    $scope.newUserAnswer = "";
                }
            };

            function getQuestion(idProduct, idQuestion) {
                UserQuestionsFactory.getQuestion(idProduct, idQuestion).then(function (data) {
                    $scope.support = data.data.product.filter(function(obj) {
                        if (obj._id === idQuestion) {
                            return obj;
                        }
                    });
                });
            }
        }
    ]);