var myApp = angular.module('ngclient',
        [
          'ngRoute',
          'LiveSearch',
          'shopFilters',
          'angularUtils.directives.dirPagination',
          'ngCart',
          'formly',
          'formlyBootstrap',
          'angularFileUpload',
          'googlechart',
          'angular.filter'
        ]);

myApp.constant('config', {
  apiUrl: '/api/v1',
  appVersion: 1.0
});

myApp.run(function($rootScope, $templateCache) {
  $rootScope.$on('$viewContentLoaded', function() {
    $templateCache.removeAll();
  });
});

myApp.config(function($routeProvider, $httpProvider) {
  $httpProvider.interceptors.push('TokenInterceptor');
  $routeProvider
      .when('/', {
        templateUrl: 'partials/home.html',
        controller: 'HomeCtrl',
        access: {
          requiredLogin: false
        }
      }).when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'LoginCtrl',
        access: {
          requiredLogin: false
        }
      }).when('/registration', {
        templateUrl: 'partials/registration.html',
        controller: 'LoginCtrl',
        access: {
          requiredLogin: false
        }
      }).when('/cart', {
        template: '<div class="cart-content"><ngcart-cart></ngcart-cart></div>',
        controller: 'CartController',
        access: {
          requiredLogin: false
        }
      }).when('/product/:id', {
        templateUrl: 'partials/product.html',
        controller: 'ProductsCtrl',
        access: {
          requiredLogin: false
        }
      }).when('/profile', {
        templateUrl: 'partials/profile.html',
        controller: 'ProfileCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/profile/edit', {
        templateUrl: 'partials/edit_profile.html',
        controller: 'ProfileCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/setup', {
        templateUrl: 'partials/create_admin.html',
        controller: 'SetupCtrl',
        access: {
          requiredLogin: false
        }
      }).when('/user/questions', {
        templateUrl: 'partials/questions.html',
        controller: 'UserQuestionsCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/user/wishlist', {
        templateUrl: 'partials/wishlist.html',
        controller: 'WishlistCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/user/products/:idProduct/question/:idQuestion', {
        templateUrl: 'partials/question.html',
        controller: 'UserQuestionsCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/products', {
        templateUrl: 'partials/admin/products/list.html',
        controller: 'AdminProductsCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/product/:id/show', {
        templateUrl: 'partials/admin/products/show_product.html',
        controller: 'AdminProductsCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/product/:id/edit', {
        templateUrl: 'partials/admin/products/edit_product.html',
        controller: 'AdminProductsCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/product/new', {
        templateUrl: 'partials/admin/products/new_product.html',
        controller: 'AdminProductsCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/product/:id/delete', {
        templateUrl: 'partials/admin/products/delete_product.html',
        controller: 'AdminProductsCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/users', {
        templateUrl: 'partials/admin/users/list.html',
        controller: 'AdminUsersCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/user/:id', {
        templateUrl: 'partials/admin/users/show_user.html',
        controller: 'AdminUsersCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/user/:id/edit', {
        templateUrl: 'partials/admin/users/edit_user.html',
        controller: 'AdminUsersCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/users/new', {
        templateUrl: 'partials/admin/users/new_user.html',
        controller: 'AdminUsersCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/orders', {
        templateUrl: 'partials/admin/orders/list.html',
        controller: 'AdminOrdersCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/order/:id', {
        templateUrl: 'partials/admin/orders/show_order.html',
        controller: 'AdminOrdersCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/user/order/:id', {
        templateUrl: 'partials/admin/orders/show_order.html',
        controller: 'AdminOrdersCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/questions', {
        templateUrl: 'partials/admin/admin_questions.html',
        controller: 'AdminQuestionsCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/products/:idProduct/question/:idQuestion', {
        templateUrl: 'partials/admin/admin_question.html',
        controller: 'AdminQuestionCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/categories', {
        templateUrl: 'partials/admin/categories/list.html',
        controller: 'AdminCategoriesCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/categories/:id/edit', {
        templateUrl: 'partials/admin/categories/edit_category.html',
        controller: 'AdminCategoriesCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/categories/new', {
        templateUrl: 'partials/admin/categories/new_category.html',
        controller: 'AdminCategoriesCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/categories/:id/delete', {
        templateUrl: 'partials/admin/categories/delete_category.html',
        controller: 'AdminCategoriesCtrl',
        access: {
          requiredLogin: true
        }
      }).when('/admin/reports/', {
        templateUrl: 'partials/admin/reports/show.html',
        controller: 'AdminReportsCtrl',
        access: {
          requiredLogin: true
        }
      }).otherwise({
        redirectTo: '/'
      });
});
myApp.run(function($rootScope, $window, $location, AuthenticationFactory, formlyConfig) {
  formlyConfig.setType({
    name: 'toggleBox',
    template: '<input type="checkbox" toggle-checkbox ng-model="model[options.key]" toggle-checkbox' +
                  'data-size="mini" data-toggle="toggle" data-on="banned" data-off="not banned"' +
                  'data-onstyle="danger" data-offstyle="success">'
  });

  formlyConfig.setWrapper({
    name: 'loading',
    templateUrl: 'loading.html'
  });
// when the page refreshes, check if the user is already logged in
//  AuthenticationFactory.check();
  $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
    AuthenticationFactory.check();
    if ((nextRoute.access && nextRoute.access.requiredLogin) && !AuthenticationFactory.isLogged) {
      $location.path("/login");
    } else {
// check if user object exists else fetch it. This is incase of a page refresh
      if (!AuthenticationFactory.user) AuthenticationFactory.user = $window.sessionStorage.user;
      if (!AuthenticationFactory.admin) AuthenticationFactory.isAdmin = $window.sessionStorage.admin;
    }
  });
  $rootScope.$on('$routeChangeSuccess', function(event, nextRoute, currentRoute) {
    AuthenticationFactory.check();
    $rootScope.showMenu = AuthenticationFactory.isLogged;
    $rootScope.showForAdmin = AuthenticationFactory.isAdmin;
// if the user is already logged in, take him to the home page
    if (AuthenticationFactory.isLogged && $location.path() == '/login') {
      $location.path('/');
    }
  });
});