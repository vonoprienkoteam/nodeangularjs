//(function () {
//
//    "use strict";
//
//    angular
//        .module('app')
//        .config(config);
//
//    config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider'];
//    function config($routeProvider, $locationProvider, $httpProvider) {
//
//        $locationProvider.hashPrefix('');
//
//        $httpProvider.interceptors.push('TokenInterceptor');
//
//        $routeProvider
//        .when('/', {
//            templateUrl: 'partials/index.html',
//            controller: 'mainController',
//            controllerAs: 'main',
//            access: {
//                requiredLogin: false
//            }
//        })
//        .when('/product/:id', {
//            templateUrl: 'js/template/product.html',
//            controller: 'productController',
//            controllerAs: 'product',
//            access: {
//                requiredLogin: false
//            }
//        })
//        .when('/login', {
//            templateUrl: 'partials/login.html',
//            controller: 'LoginCtrl',
//            access: {
//                requiredLogin: false
//            }
//        })
//        .when('/page1', {
//            templateUrl: 'partials/page1.html',
//            controller: 'Page1Ctrl',
//            access: {
//                requiredLogin: true
//            }
//        })
//        .when('/registration', {
//            templateUrl: 'js/template/registration.html',
//            controller: 'registrationController',
//            controllerAs: 'registration'
//        })
//        .otherwise({
//            redirectTo: '/login'
//        });
//}
//
//})();

myApp.factory('flash', function($rootScope) {
    var queue = [], currentMessage = '';

    $rootScope.$on('$routeChangeSuccess', function() {
        if (queue.length > 0)
            currentMessage = queue.shift();
        else
            currentMessage = '';
    });

    return {
        set: function(message) {
            queue.push(message);
        },
        get: function(message) {
            return currentMessage;
        }
    };
});

myApp.factory('', function() {

});