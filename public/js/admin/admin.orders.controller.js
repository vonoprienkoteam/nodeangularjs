myApp.controller("AdminOrdersCtrl", ['$scope', 'OrdersFactory', '$routeParams', '$location',

  function ($scope, OrdersFactory, $routeParams, $location) {
    var id = $routeParams.id;

    if (id) {
      OrdersFactory.getOne(id)
          .then(function (response) {
            $scope.order = response.data.order;
          }, function (status) {
            console.log(status);
          });
    }

    OrdersFactory.getAll()
        .then(function (response) {
          $scope.orders = response.data.orders
        }, function (status) {
          console.log(status);
        });

    $scope.sort = function (keyname) {
      $scope.sortType = keyname;   //set the sortKey to the param passed
      $scope.sortReverse = !$scope.sortReverse; //if true make it false and vice versa
    };

    $scope.gotoList = function () {
      $location.path('/admin/orders/')
    };

    $scope.togglePayment = function (order) {
      OrdersFactory.updateOrder(order._id, order.paid)
          .then(function (response) {
            order.paid = response.data.order.paid;
          }, function (status) {
            console.log(status);
          });
    };

    $scope.sortType = 'createdAt'; // set the default sort type
    $scope.sortReverse = true;  // set the default sort order
    $scope.searchOrder = '';     // set the default search/filter term

  }

]);