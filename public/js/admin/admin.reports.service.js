myApp.service('AdminReportService', function ($http, config) {
  var urlBase = config.apiUrl + '/admin/reports';

  this.getOrdersStats = function () {
    return $http.get(urlBase + '/orders');
  };

  this.getCategoriesStats = function () {
    return $http.get(urlBase + '/categories');
  };

  this.getProductsStats = function () {
    return $http.get(urlBase + '/products');
  };

  this.getUsersStats = function () {
    return $http.get(urlBase + '/users');
  };

  this.getSalesStats = function () {
    return $http.get(urlBase + '/sales');
  };
});