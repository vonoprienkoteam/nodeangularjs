myApp.controller("AdminReportsCtrl", ['$scope', 'AdminReportService',

  function ($scope, AdminReportService) {

    $scope.cssStyle = "min-height:500px; width:100%;";

    /*
    * Orders section
    */
    $scope.ordersChart = {};
    $scope.ordersChart.type = "PieChart";
    $scope.ordersChart.options = {
      'title': 'Statistics of orders',
      is3D: true
    };
    AdminReportService.getOrdersStats().then(function (response) {
      $scope.ordersChart.data = response.data.stats;
    }, function (status) {
      console.log(status);
    });



    /*
    * Categories section
    */
    $scope.categoriesChart = {};
    $scope.categoriesChart.type = "ColumnChart";
    $scope.categoriesChart.options = {
      'title': 'Products for each category',
      is3D: true
    };

    AdminReportService.getCategoriesStats().then(function (response) {
      $scope.categoriesChart.data = response.data.stats;
    }, function (status) {
      console.log(status);
    });

    /*
    * Products section
    */
    $scope.productsChart = {};
    $scope.productsChart.type = "BarChart";
    $scope.productsChart.options = {
      'title': 'Most popular products',
      is3D: true
    };
    AdminReportService.getProductsStats().then(function (response) {
      $scope.productsChart.data = response.data.stats;
    }, function (status) {
      console.log(status);
    });

    $scope.salesChart = {};
    $scope.salesChart.type = "AnnotationChart";
    $scope.salesChart.options = {
      'title': 'Sales info',
      displayAnnotations: false
    };
    AdminReportService.getSalesStats().then(function (response) {
      response.data.stats.rows.forEach(function(value){
        value.c[0].v = new Date(value.c[0].v);
      });
      $scope.salesChart.data = response.data.stats;
    }, function (status) {
      console.log(status);
    });

  }
]);