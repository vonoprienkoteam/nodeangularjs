myApp.factory('OrdersFactory', function($http, config) {
    var urlBase = config.apiUrl +'/admin/order';
    var _orderFactory = {};
    _orderFactory.getAll = function() {
        return $http.get(urlBase + 's'); // api/v1/admin/orders
    };

    _orderFactory.getOne = function(id) {
        return $http.get(urlBase +'/' + id); // api/v1/admin/order/:id
    };

    _orderFactory.updateOrder = function(id, bool) {
        var data = JSON.stringify({ paid: bool });
        return $http.put(urlBase +'/' + id, data); // api/v1/admin/order/:id
    };

    return _orderFactory;
});