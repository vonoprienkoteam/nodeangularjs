myApp.controller('AdminQuestionCtrl',
    ['$scope', 'AuthenticationFactory', 'QuestionsFactory', '$routeParams',

        function ($scope, AuthenticationFactory, QuestionsFactory, $routeParams) {

            var idQuestion = $routeParams.idQuestion,
                idProduct = $routeParams.idProduct;

            $scope.isLogged = AuthenticationFactory.isLogged;
            $scope.isAdmin = AuthenticationFactory.isAdmin;
            $scope.userId = AuthenticationFactory.userId;
            $scope.newAnswer = "";

            if ($scope.isAdmin) {
                    getQuestion(idProduct, idQuestion);
            }

            $scope.createAnswer = function () {
                if ($scope.newAnswer) {
                    QuestionsFactory.addAnswer(idProduct, $scope.newAnswer, $scope.isAdmin, idQuestion)
                        .then(function (response) {
                            $scope.questionLabel = "Thank you for answer.";
                            getQuestion(idProduct, idQuestion);
                        }, function (status) {
                            //console.log(status);
                        });
                    $scope.newAnswer = "";
                }
            };
            function getQuestion(idProduct, idQuestion) {
                QuestionsFactory.getQuestion(idProduct, idQuestion).then(function (data) {
                    $scope.support = data.data.product.filter(function(obj) {
                        if (obj._id === idQuestion) {
                            return obj;
                        }
                    });
                    //console.log($scope.support);
                });
            }
        }
    ]);