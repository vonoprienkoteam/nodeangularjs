myApp.controller("AdminCategoriesCtrl", ['$scope', '$routeParams', 'AdminCategoriesService', '$location', '$window',

  function ($scope, $routeParams, AdminCategoriesService, $location, $window) {

    $scope.categories = [];
    $scope.categoriesPerPage = 24;

    var getId = function () {
      return $routeParams.id;
    };

    var getAll = function (page) {
      AdminCategoriesService.getAll(page).then(function (response) {
        $scope.categories = response.data.categories;
        $scope.totalCategories = response.data.count
      }, function (status) {
        console.log(status);
      });
    };

    var getOne = function (id) {
      if (id) {
        AdminCategoriesService.getOne(id).then(function (response) {
          $scope.category = response.data.category;
        }, function (status) {
          console.log(status);
        });
      }
    };

    getOne(getId());
    getAll();

    $scope.pageChanged = function (newPageNumber, oldPageNumber) {
      getAll(newPageNumber);
    };

    $scope.sort = function (keyname) {
      $scope.sortType = keyname;   //set the sortKey to the param passed
      $scope.sortReverse = !$scope.sortReverse; //if true make it false and vice versa
    };

    $scope.gotoDelete = function (id) {
      $location.path('/admin/categories/' + id + '/delete')
    };

    $scope.gotoList = function () {
      $location.path('/admin/categories/')
    };

    $scope.back = function () {
      $window.history.back();
    };

    $scope.deleteOne = function (id) {
      AdminCategoriesService.delete(id)
          .then(function () {
            getAll();
            $scope.gotoList();
          }, function (status) {
            console.log(status);
          });
    };

    $scope.create = function (callback) {
      var data = JSON.stringify({
        name: this.category.name
      });
      AdminCategoriesService.create(data).then(function (response) {
        $scope.category = response.data.category;
        if ( callback ) {
          $scope.category = {};
          callback()
        } else {
          $scope.gotoList()
        }
      }, function (status) {
        console.log(status);
      });
    };

    $scope.update = function () {
      var data = JSON.stringify({
        //id: this.category._id,
        name: this.category.name
      });
      AdminCategoriesService.update(this.category._id, data).then(function (response) {
        $scope.category = response.data.category;
        $scope.gotoList()
      }, function (status) {
        console.log(status);
      });
    };

    $scope.saveAndCreate = function () {
      $scope.create($scope.gotoCreate);
    };

    $scope.gotoEdit = function (id) {
      $location.path('/admin/categories/' + id + '/edit')
    };

    $scope.gotoCreate = function () {
      $location.path('/admin/categories/new')
    };

    /**
     * Formly settingsin
     **/
    $scope.categoryFields = [
      {
        // the key to be used in the model values
        // so this will be bound to $scope.category.name
        key: 'name',
        type: 'input',
        templateOptions: {
          label: 'Name',
          required: true,
          description: 'Name of category'
        }
      }
    ];

  }
]);