myApp.service('UserService', function ($http, config) {
  var urlBase = config.apiUrl + '/admin/user';

  this.getAll = function () {
    return $http.get(urlBase + 's');
  };

  this.getOne = function (id) {
    return $http.get(urlBase + '/' + id);
  };

  this.update = function (id, data) {
    return $http.put(urlBase + '/' + id, data);
  };

  this.create = function (data) {
    console.log(data);
    return $http.post(urlBase + 's', data);
  };

});