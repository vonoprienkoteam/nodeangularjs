myApp.factory('AdminProductsFactory', function ($http, config) {
  var urlBase = config.apiUrl + '/admin/product';
  var _productFactory = {};
  _productFactory.getAll = function (page) {
    return $http.get(urlBase + 's?page=' + page);
  };

  _productFactory.getOne = function (id) {
    return $http.get(config.apiUrl + '/product/' + id);
  };

  _productFactory.deletePhoto = function (filename) {
    var product_id = filename.split("-")[0];
    return $http.delete(urlBase + '/' + product_id + '/thumb/' + filename);
  };

  _productFactory.save = function (data) {
    return $http.post(urlBase, data);
  };

  _productFactory.update = function (id, data) {
    return $http.put(urlBase + '/' + id, data);
  };

  _productFactory.toggleStock = function (data) {
    console.log(data);
    return $http.put(urlBase + 's', data);
  };

  return _productFactory;
});
