myApp.controller("AdminProductsCtrl", ['$scope', '$routeParams', 'AdminProductsFactory', '$location', '$window', 'config', 'FileUploader',

  function ($scope, $routeParams, AdminProductsFactory, $location, $window, config, FileUploader) {

    var urlBase = config.apiUrl + '/admin/product';

    $scope.products = [];
    $scope.totalProducts = 0;
    $scope.productsPerPage = 24;
    $scope.pagination = {
        current: 1
    };

    var getId = function () {
      return $routeParams.id;
    };

    var getAll = function (page) {
      AdminProductsFactory.getAll(page).then(function (response) {
            $scope.products = response.data.products;
            $scope.totalProducts = response.data.count
          }, function (status) {
            console.log(status);
          });
    };

    var getOne = function (id) {
      if (id) {
        AdminProductsFactory.getOne(id).then(function (response) {
              $scope.product = response.data.product;
            }, function (status) {
              console.log(status);
            });
      }
    };

    getOne(getId());
    getAll();

    $scope.pageChanged = function (newPageNumber, oldPageNumber) {
      getAll(newPageNumber);
    };

    $scope.sort = function (keyname) {
      $scope.sortType = keyname;   //set the sortKey to the param passed
      $scope.sortReverse = !$scope.sortReverse; //if true make it false and vice versa
    };

    $scope.gotoDelete = function (id) {
      $location.path('/admin/product/' + id + '/delete')
    };

    $scope.gotoList = function () {
      if ( $location.path().indexOf('/admin/products') < 0) $location.path('/admin/products')
    };

    $scope.back = function () {
      $window.history.back();
    };

    $scope.bulkDelete = function (array) {
      for (var index = 0; index < array.length; ++index) {
        console.log(array[index]);
        //}
        AdminProductsFactory.delete(array[index]).then(function () {
              getAll();
              $scope.gotoList();
            }, function (status) {
              console.log(status);
            });
      }
    };

    $scope.bulkToggleStock = function (array, bool) {
      //for (var index = 0; index < array.length; ++index) {
      //  console.log(array[index]);
      //  //}
      var data = JSON.stringify({
        array: array,
        isInStock: bool
      });
        AdminProductsFactory.toggleStock(data).then(function () {
          getAll();
          $scope.gotoList();
        }, function (status) {
          console.log(status);
        });
      //}
    };

    $scope.save = function (callback) {
      var data = JSON.stringify({
        name: this.product.name,
        description: this.product.description,
        price: this.product.price,
        category: this.product.category
      });
      AdminProductsFactory.save(data).then(function (response) {
            $scope.product = response.data.product;
            if ( callback ) {
              $scope.product = {};
              callback()
            } else {
              $scope.gotoList()
            }
          }, function (status) {
            console.log(status);
          });
    };

    $scope.update = function () {
      var data = JSON.stringify({
        //id: this.product._id,
        name: this.product.name,
        description: this.product.description,
        price: this.product.price,
        category: this.product.category && this.product.category.name,
        isInStock: this.product.isInStock
      });
      AdminProductsFactory.update(this.product._id, data).then(function (response) {
        $scope.product = response.data.product;
          $scope.gotoList()
      }, function (status) {
        console.log(status);
      });
    };

    $scope.saveAndCreate = function () {
      $scope.save($scope.gotoCreate);
    };

    $scope.gotoShow = function (id) {
      $location.path('/product/' + id)
    };

    $scope.gotoEdit = function (id) {
      $location.path('/admin/product/' + id + '/edit')
    };

    $scope.gotoCreate = function () {
      $location.path('/admin/product/new')
    };


    // Bulk action
    $scope.selection = [];
    // toggle selection for a given product by id
    $scope.toggleSelection = function toggleSelection(productId) {
      if (productId) {
        var idx = $scope.selection.indexOf(productId);
        // is currently selected
        if (idx > -1) {
          $scope.selection.splice(idx, 1);
        }
        // is newly selected
        else {
          $scope.selection.push(productId);
        }
      } else {
        if ($scope.selection.length == $scope.products.length) {
          $scope.selection = []
        } else {
          $scope.selection = [];
          angular.forEach($scope.products, function (key, value, obj) {
            this.push(key._id);
          }, $scope.selection);
        }
      }
    };

    /**
     * Formly settingsin
     **/
    $scope.productFields = [
      {
        // the key to be used in the model values
        // so this will be bound to $scope.product.name
        key: 'name',
        className: 'col-sm-6',
        type: 'input',
        templateOptions: {
          label: 'Name',
          placeholder: 'transistor',
          required: true,
          description: 'Name of product'
        }
      },
      {
        key: 'price',
        className: 'col-sm-6',
        type: 'input',
        templateOptions: {
          label: 'Price',
          required: true,
          step: 0.1,
          min: 0.1,
          max: 100000,
          maxlength: 6,
          type: 'number',
          description: 'Price should be less then 100,000'
        }
      },
      {
        key: 'description',
        className: 'col-sm-12',
        type: 'textarea',
        templateOptions: {
          rows: 6,
          //type: 'textarea',
          label: 'Description',
          placeholder: 'transistor',
          required: true,
          description: 'Short description of a product'
        }
      },
      {
        key: 'isInStock',
        className: 'col-sm-6',
        defaultValue: true,
        type: 'select',
        templateOptions: {
          label: 'Stock status',
          required: true,
          options: [
            {name: 'in stock', value: true},
            {name: 'out of stock', value: false}
          ]
        }
      },
      {
        key: 'category',
        className: 'col-sm-6',
        type: 'select',
        wrapper: "loading",
        templateOptions: {
          label: 'Category',
          options: [],
          //required: true,
          valueProp: 'name',
          labelProp: 'name'
        },
        controller: function($scope, AdminCategoriesService, productsFactory, $routeParams) {
          $scope.to.loading = AdminCategoriesService.getAll().then(function(response){
            $scope.to.options = response.data.categories;
            // note, the line above is shorthand for:
            // $scope.options.templateOptions.options = data;
            productsFactory.getProduct($routeParams.id).then(function(response) {
              if (response.data.product.category) {
                $scope.model.category = response.data.product.category.name;
              }
            });
            return response;
          });
        }
      }
    ];

    $scope.showModal = false;
    $scope.toggleModal = function(){
      $scope.showModal = !$scope.showModal;
    };

    var uploader = $scope.uploader = new FileUploader({
      headers : {
        'x-access-token': $window.sessionStorage.token
      },
      url: urlBase + '/' + getId() + '/thumbs',
      queueLimit: 5
    });

    // FILTERS

    uploader.filters.push({
      name: 'imageFilter',
      fn: function(item /*{File|FileLikeObject}*/, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    uploader.onCompleteAll = function () {
      getOne(getId());
      //uploader.clearQueue();
    };

    $scope.deletePhoto = function (filename) {
      AdminProductsFactory.deletePhoto(filename).then(function () {
        getOne(getId());
      }, function (status) {
        console.log(status);
      });
    }

  }
]);