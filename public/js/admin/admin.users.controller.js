myApp.controller("AdminUsersCtrl", ['$scope', 'UserService', '$routeParams', '$location', '$window',

  function ($scope, UserService, $routeParams, $location, $window) {

    var getId = function () {
      return $routeParams.id;
    };

    var getAll = function () {
      UserService.getAll()
          .then(function (response) {
            $scope.users = response.data.users
          }, function (status) {
            console.log(status);
          });
    };

    var getOne = function (id) {
      if (id) {
        UserService.getOne(id)
            .then(function (response) {
              $scope.user = response.data.user;
            }, function (status) {
              console.log(status);
            });
      }
    };

    getOne(getId());
    getAll();

    $scope.sort = function (keyname) {
      $scope.sortType = keyname;   //set the sortKey to the param passed
      $scope.sortReverse = !$scope.sortReverse; //if true make it false and vice versa
    };

    $scope.gotoList = function () {
      $location.path('/admin/users/')
    };

    $scope.back = function () {
      $window.history.back();
    };

    $scope.create = function () {
      var data = JSON.stringify({
        name: this.user.name,
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        email: this.user.email,
        gender: this.user.gender,
        admin: this.user.admin,
        password: this.user.password,
        passwordConfirmation: this.user.passwordConfirmation
      });
      if (this.userForm.$valid) {
        UserService.create(data)
            .then(function (response) {
              $scope.user = response.data.user;
              $scope.gotoList()
            }, function (status) {
              console.log(status);
            });
      }
    };

    $scope.save = function () {
      var data = JSON.stringify({
        firstName: this.user.firstName,
        lastName: this.user.lastName,
        email: this.user.email,
        phone: this.user.phone,
        gender: this.user.gender,
        admin: this.user.admin,
        banned: this.user.banned
      });
      if (this.userForm.$valid) {
        UserService.update(getId(), data)
            .then(function (response) {
              $scope.user = response.data.user;
              $scope.gotoList()
            }, function (status) {
              console.log(status);
            });
      }
    };

    $scope.gotoShow = function (id) {
      $location.path('/admin/user/' + id)
    };

    $scope.gotoEdit = function (id) {
      $location.path('/admin/user/' + id + '/edit')
    };

    $scope.gotoCreate = function () {
      $location.path('/admin/users/new')
    };

    /**
     * Formly settingsin
     **/
    $scope.userFields = [
      {
        key: 'name',
        className: 'col-sm-6',
        type: 'input',
        templateOptions: {
          label: 'User name',
          required: true
        },
        hideExpression: 'model._id'
      },
      {
        key: 'admin',
        className: 'col-sm-6',
        type: 'select',
        templateOptions: {
          label: 'Role',
          required: true,
          options: [
            {name: 'admin', value: 'true'},
            {name: 'user', value: 'false'}
          ]
        }
      },
      {
        key: 'password',
        className: 'col-sm-6',
        type: 'input',
        templateOptions: {
          label: 'Password',
          type: 'password',
          required: true
        },
        hideExpression: 'model._id'
      },
      {
        key: 'passwordConfirmation',
        className: 'col-sm-6',
        type: 'input',
        templateOptions: {
          label: 'Confirm password',
          placeholder: 'Please re-enter user password',
          type: 'password',
          required: true
        },
        hideExpression: 'model._id'
      },
      {
        key: 'firstName',
        className: 'col-sm-6',
        type: 'input',
        templateOptions: {
          label: 'First name'
        }
      },
      {
        key: 'lastName',
        className: 'col-sm-6',
        type: 'input',
        templateOptions: {
          label: 'Last name'
        }
      },
      {
        key: 'email',
        className: 'col-sm-6',
        type: 'input',
        templateOptions: {
          label: 'Email'
        }
      },
      {
        key: 'gender',
        className: 'col-sm-4',
        type: 'select',
        templateOptions: {
          label: 'Gender',
          options: [
            {name: 'male', value: 'male'},
            {name: 'female', value: 'female'}
          ],
          description: 'It is not necessary'
        }
      },
      {
        key: 'phone',
        className: 'col-sm-4',
        type: 'input',
        templateOptions: {
          label: 'Phone'
        }
      },
      {
        key: 'banned',
        className: 'col-sm-4 toggle-box',
        type: 'toggleBox',
        templateOptions: {
          label: 'Banned'
        }
      }
    ];

  }
]);