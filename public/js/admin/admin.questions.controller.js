myApp.controller('AdminQuestionsCtrl',
    ['$scope', 'AuthenticationFactory', 'QuestionsFactory', '$routeParams',

        function ($scope, AuthenticationFactory, QuestionsFactory, $routeParams) {

            var id = $routeParams.id;
            $scope.isLogged = AuthenticationFactory.isLogged;
            $scope.isAdmin = AuthenticationFactory.isAdmin;
            $scope.userId = AuthenticationFactory.userId;
            $scope.newQuestion = "";

            if ($scope.isAdmin) {
                QuestionsFactory.getQuestions().then(function (data) {
                    $scope.products = data.data.products;
                });
            } else {
                if ($scope.userId) {
                    QuestionsFactory.getUserQuestions($scope.userId).then(function (data) {
                        $scope.products = data.data.products;
                    });
                }
            }

            $scope.sendQuestion = function () {
                if ($scope.newQuestion) {
                    QuestionsFactory.addQuestion(id, $scope.newQuestion)
                        .then(function (response) {
                            $scope.questionLabel = "Thank you for your question.";
                            //var askField = document.getElementById("question");
                            //we can hide fields for ask question
                            // askField.style.display = (askField.style.display == 'none') ? '' : 'none';
                        }, function (status) {
                            //console.log(status);
                        });

                    $scope.newQuestion = "";
                }
            };

        }
    ]);