myApp.service('AdminCategoriesService', function($http, config) {
  var urlBase = config.apiUrl +'/admin/categories';

  this.getAll = function() {
      return $http.get(urlBase);
  };

  this.getOne = function(id) {
    return $http.get(urlBase +'/' + id);
  };

  this.delete = function(categoryId) {
    return $http.delete(urlBase + '/' + categoryId);
  };

  this.create = function(data) {
      return $http.post(urlBase, data);
  };

  this.update = function(id, data) {
    return $http.put(urlBase +'/' + id, data);
  };

});
