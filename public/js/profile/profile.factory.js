myApp.factory('profileFactory', function($http, config) {
    /** https://docs.angularjs.org/guide/providers **/
    var urlBase = config.apiUrl + '/user';
    var _profFactory = {};

    _profFactory.update = function(userData) {
        return $http.put(urlBase, userData);
    };

    _profFactory.read = function() {
        return $http.get(urlBase)
    };

    return _profFactory;
});