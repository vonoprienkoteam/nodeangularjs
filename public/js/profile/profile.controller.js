myApp.controller("ProfileCtrl", ['$scope', 'profileFactory', '$window', '$location', '$http', 'config', 'FileUploader',
  function ($scope, profileFactory, $window, $location, $http, config, FileUploader) {
    var urlBase = config.apiUrl + '/user';
    $scope.name = "Profile";
    //var user = $window.sessionStorage.user;
    var readUserInfo = function () {
      profileFactory.read().then(function (response) {
            $scope.user = response.data.user;
          },
          function (status) {
            $scope.errmessage = status.message;
          });
    };

    readUserInfo();

    $scope.updateProfile = function () {
      console.log(this.user);
      var data = JSON.stringify({
        lastName: this.user.lastName,
        firstName: this.user.firstName,
        email: this.user.email,
        phone: this.user.phone,
        gender: this.user.gender,
        newPassword: this.user.newPassword,
        confirmNewPassword: this.user.confirmNewPassword
      });
      profileFactory.update(data).then(function () {
        readUserInfo();
        console.log(JSON.stringify($scope.user, undefined, 2));
        $window.sessionStorage.phone = $scope.user.phone;
        $location.path("/profile");
      }, function (status) {
        if (data.newPassword != data.confirmNewPassword) {
          alert('Passwords are not equal!');
        } else {
          $scope.errmessage = status.message;
        }
      });
    };

    $scope.createAdmin = function () {
      $http.get(config.apiUrl + '/setup').then(function (response) {
        alert(response.data.message);
        $location.path("/");
      }, function (status) {
        alert(status.message);
      });
    };

    $scope.back = function () {
      $window.history.back();
    };

    $scope.showModal = false;
    $scope.toggleModal = function () {
      $scope.showModal = !$scope.showModal;
    };

    var uploader = $scope.uploader = new FileUploader({
      headers: {
        'x-access-token': $window.sessionStorage.token
      },
      url: urlBase + '/avatar',
      queueLimit: 1
    });

    // FILTERS

    uploader.filters.push({
      name: 'imageFilter',
      fn: function (item /*{File|FileLikeObject}*/, options) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
      }
    });

    // CALLBACKS
    //
    //uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
    //    console.info('onWhenAddingFileFailed', item, filter, options);
    //};
    //uploader.onAfterAddingFile = function(fileItem) {
    //    console.info('onAfterAddingFile', fileItem);
    //};
    //uploader.onAfterAddingAll = function(addedFileItems) {
    //    console.info('onAfterAddingAll', addedFileItems);
    //};
    //uploader.onBeforeUploadItem = function(item) {
    //    console.info('onBeforeUploadItem', item);
    //};
    //uploader.onProgressItem = function(fileItem, progress) {
    //    console.info('onProgressItem', fileItem, progress);
    //};
    //uploader.onProgressAll = function(progress) {
    //    console.info('onProgressAll', progress);
    //};
    //uploader.onSuccessItem = function(fileItem, response, status, headers) {
    //    console.info('onSuccessItem', fileItem, response, status, headers);
    //};
    //uploader.onErrorItem = function(fileItem, response, status, headers) {
    //    console.info('onErrorItem', fileItem, response, status, headers);
    //};
    //uploader.onCancelItem = function(fileItem, response, status, headers) {
    //    console.info('onCancelItem', fileItem, response, status, headers);
    //};
    //uploader.onCompleteItem = function(fileItem, response, status, headers) {
    //    console.info('onCompleteItem', fileItem, response, status, headers);
    //};
    uploader.onCompleteAll = function () {
      readUserInfo();
      uploader.clearQueue();
      $scope.toggleModal();
      //console.info('onCompleteAll');
    };
    //
    //console.info('uploader', uploader);

    /**
     * Formly settingsin
     **/

    $scope.userFields = [
      {
        key: 'firstName',
        className: 'col-md-6',
        type: 'input',
        templateOptions: {
          label: 'First name'
        }
      },
      {
        key: 'lastName',
        className: 'col-md-6',
        type: 'input',
        templateOptions: {
          label: 'Last name'
        }
      },
      {
        key: 'email',
        className: 'col-md-4',
        type: 'input',
        templateOptions: {
          label: 'Email'
        }
      },
      {
        key: 'phone',
        className: 'col-md-4',
        type: 'input',
        templateOptions: {
          label: 'Phone'
        }
      },
      {
        key: 'gender',
        className: 'col-md-4',
        type: 'select',
        templateOptions: {
          label: 'Gender',
          options: [
            {name: 'male', value: 'male'},
            {name: 'female', value: 'female'}
          ],
          description: 'It is not necessary'
        }
      },
      {
        key: 'newPassword',
        className: 'col-md-6',
        type: 'input',
        templateOptions: {
          label: 'New password',
          type: 'password',
          minlength: 3
        }
      },
      {
        key: 'confirmNewPassword',
        className: 'col-md-6',
        type: 'input',
        templateOptions: {
          label: 'Confirm password',
          placeholder: 'Please re-enter your password',
          type: 'password'
        }
      }
    ];
  }
]);
