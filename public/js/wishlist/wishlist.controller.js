myApp.controller("WishlistCtrl", ['$scope', 'AuthenticationFactory', 'UserWishlistFactory',
    function ($scope, AuthenticationFactory, UserWishlistFactory) {

        var userId = AuthenticationFactory.userId;

        UserWishlistFactory.getWishlist(userId).then(function (data) {
            $scope.products = data.data.products;
        });

        $scope.deleteFromWishlist = function (id) {
            UserWishlistFactory.deleteOneWishlist(id, userId).then(function (data) {
                $scope.products = data.data.products;
            });
        };

    }
]);