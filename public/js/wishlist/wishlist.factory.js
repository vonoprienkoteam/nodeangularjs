myApp.factory('UserWishlistFactory', function ($http, config) {

    var _wishlistFactory = {};

    _wishlistFactory.getWishlist = function (userId) {
        return $http.get(config.apiUrl + '/products/wishlist/' + userId);
    };

    _wishlistFactory.deleteOneWishlist = function (id, userId) {
        return $http.delete(config.apiUrl + '/products/wishlist/' + userId + '/' + id);
    };

    return _wishlistFactory;
});