myApp.controller("ProductsCtrl", ['$scope', 'productsFactory', 'AuthenticationFactory', '$routeParams', '$interval', 'productService',

  function ($scope, productsFactory, AuthenticationFactory, $routeParams, $interval, productService) {

    var id = $routeParams.id;
    $scope.products = [];
    $scope.product = {};
    $scope.newComment = "";
    $scope.wishlistNotice = "";
    $scope.isLogged = AuthenticationFactory.isLogged;
    $scope.isAdmin = AuthenticationFactory.isAdmin;
    $scope.userId = AuthenticationFactory.userId;

    var reviewProd = sessionStorage.getItem("reviewed");
    if (reviewProd == null) {
      sessionStorage.setItem("reviewed", id);
    } else {
      if (reviewProd.indexOf(id) === -1) {
        $scope.arr = reviewProd.split(',');
        if ($scope.arr.length >= 15) {
          $scope.arr.shift();
          $scope.arr.push(id);
          $scope.arr = unique($scope.arr);
          sessionStorage.setItem("reviewed", $scope.arr);
        } else {
          sessionStorage.setItem("reviewed", reviewProd + ',' + id);
        }
      } else {
        $scope.arr = sessionStorage.getItem("reviewed").split(',');
      }
    }

    var tick = function () {
      $scope.clock = Date.now();
    };
    tick();
    $interval(tick, 1000);

    $scope.createComment = function () {
      if ($scope.newComment) {
        productsFactory.addComment(id, $scope.newComment)
            .then(function (response) {
              $scope.product.comments = response.data.product.comments
            }, function (status) {
              console.log(status);
            });
        $scope.newComment = "";
      }
    };

    $scope.deleteComment = function (commentId) {
      productsFactory.deleteComment(id, commentId)
          .then(function (response) {
            $scope.product.comments = response.data.product.comments
          }, function (status) {
            console.log(status);
          });
    };

    $scope.updateComment = function (commentId, newText) {
      productsFactory.updateComment(id, commentId, newText)
          .then(function (response) {
            $scope.product.comments = response.data.product.comments
          }, function (status) {
            console.log(status);
          });
    };

    // Access the factory and get products list
    productsFactory.getProducts().then(function (data) {
      $scope.products = data.data;
    });

    productsFactory.getProduct(id).then(function (data) {
      $scope.product = data.data.product;
      if (data.data.product.images) $scope.mainImageUrl = data.data.product.images[0];
    });

    $scope.setImage = function (imageUrl) {
      $scope.mainImageUrl = imageUrl;
    };


//<<<<<<< HEAD
//    var reviewed = function () {
//      productService.getReviewed($scope.arr)
//          .then(function (response) {
//            $scope.reViewed = response.data.products;
//            var swiper = new Swiper('.swiper-container', {
//              pagination: '.swiper-pagination',
//              slidesPerView: 5,
//              paginationClickable: true,
//              nextButton: '.swiper-button-next',
//              prevButton: '.swiper-button-prev',
//              spaceBetween: 30
//            });
//          }, function (status) {
//            console.log(status);
//          });
//    };
//    if ($scope.arr) {
//      reviewed();
//=======
    var reviewed = function () {
      productService.getReviewed($scope.arr)
          .then(function (response) {
            var swiper = new Swiper('.swiper-container', {
              slidesPerView: 5,
              paginationClickable: true,
              nextButton: '.swiper-button-next',
              prevButton: '.swiper-button-prev',
              spaceBetween: 30
            });
            $scope.reViewed = response.data.products;

          }, function (status) {
            console.log(status);
          });
    };
    if ($scope.arr) {
      reviewed();
    }

    $scope.addtoWishlist = function () {
      productsFactory.addtoWishlist(id, $scope.userId)
          .then(function (response) {
            //console.log(response);
          }, function (status) {
            console.log(status);
          });
      $scope.wishlistNotice = "Product added to wishlist";
    };
//>>>>>>> origin/fix-slider
//    }
//
//    $scope.addtoWishlist = function () {
//      productsFactory.addtoWishlist(id, $scope.userId)
//          .then(function (response) {
//            //console.log(response);
//          }, function (status) {
//            console.log(status);
//          });
//      $scope.wishlistNotice = "Product added to wishlist";
//    };
  }
]);

function unique(arr) {
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true;
  }
  return Object.keys(obj);
}