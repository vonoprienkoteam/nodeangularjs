myApp.service('productService', function($http, config) {
    var urlBase = config.apiUrl;

    this.getAll = function() {
        return $http.get(urlBase + 's');
    };

    this.getReviewed = function(arr) {
        var data = JSON.stringify({
            reviewed: arr
        });
        return $http.post(config.apiUrl + '/products/reviewed', data);
    };

});