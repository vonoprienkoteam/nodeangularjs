myApp.factory('productsFactory', function($http, config) {
    /** https://docs.angularjs.org/guide/providers **/
    var urlBase = config.apiUrl +'/product';
    var _prodFactory = {};
    _prodFactory.getProducts = function(page, category) {
        if (category) {
            return $http.get(urlBase + 's?page=' + page + '&category=' + category);
        } else {
            return $http.get(urlBase + 's?page=' + page);
        }
    };

    _prodFactory.getCategories = function() {
        return $http.get(urlBase + 's/categories');
    };

    _prodFactory.getProduct = function(id) {
        return $http.get(config.apiUrl +'/product/' + id);
    };

    _prodFactory.addComment = function(productId, text) {
        var data = JSON.stringify({ text: text });
        return $http.post(config.apiUrl + '/product/' + productId + '/comment', data);
    };

    _prodFactory.deleteComment = function(productId, commentId) {
        return $http.delete(config.apiUrl + '/admin/product/' + productId + '/comment/' + commentId);
    };

    _prodFactory.updateComment = function(productId, commentId, text) {
        var data = JSON.stringify({ text: text });
        return $http.put(config.apiUrl + '/product/' + productId + '/comment/' + commentId, data);
    };

    _prodFactory.addtoWishlist = function(id, userId) {
        var data = JSON.stringify({
            idProd : id,
            userId: userId
        });
        return $http.post(urlBase + 's/addtowishlist', data);
    };

    return _prodFactory;
});