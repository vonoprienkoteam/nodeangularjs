var jwt = require('jwt-simple'),
    User = require('../models/user');

module.exports = function (req, res, next) {

  // When performing a cross domain request, you will recieve
  // a preflighted request first. This is to check if our the app
  // is safe.

  // We skip the token outh for [OPTIONS] requests.
  //if(req.method == 'OPTIONS') next();
  var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];

  if (token) {
    try {
      var decoded = jwt.decode(token, require('../config.js').secret);

      if (decoded.exp <= Date.now()) {
        res.status(400).json({
          success: 400,
          message: "Token Expired"
        });
        return;
      }

      // Authorize the user to see if s/he can access our resources

      User.findOne({name: decoded.user.name}, function (err, user) {
        if (err) next(err);
        if (user) {
          dbUser = {
            name: user.name,
            admin: user.admin
          };

          if ((req.url.indexOf('admin') >= 0 && dbUser.admin == 'true') || (req.url.indexOf('admin') < 0)) {
            req.user_id = user._id;
            next(); // To move to next middleware
          } else {
            res.status(403).json({
              success: false,
              message: "Not Authorized"
            });
          }
        } else {
          // No user with this name exists, respond back with a 401
          res.status(401).json({
            success: false,
            message: "Invalid User"
          });
        }
      });

    } catch (err) {
      if (err) return next(err);
    }
  } else {
    res.status(401).json({
      success: 401,
      message: "Invalid Token or Key"
    });
    //return;
  }
};