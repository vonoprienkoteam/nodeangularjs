var express = require('express'),
    User = require('../models/user'),
    Product = require('../models/product'),
    Category = require('../models/category'),
    mongoose = require('mongoose');

const fs = require('fs');

var products = {

  getAll: function (req, res, next) {
    var category = req.query.category,
        page = req.query.page || 1,
        perPage = req.query.per || 24;

    if (category) {
      Category.findOne({name: req.query.category}, function (err, category) {
        if (err) return next(err);
        if (category) {
          Product.count({category: mongoose.Types.ObjectId(category._id)}, function (err, count) {
            if (err) return next(err);
            Product.find({
              category: mongoose.Types.ObjectId(category._id)
            }).sort("name").limit(perPage).skip(page * perPage - perPage).populate("category", "name").exec(function (err, products) {
              if (err) return next(err);
              res.json({
                success: true,
                products: products,
                count: count
              })
            });
          })
        } else {
          return res.status(404).json({
            success: false,
            message: 'Category with name ' + req.query.category + ' can not be found'
          });
        }
      })
    } else {
      Product.count({}, function (err, count) {
        if (err) return next(err);
        Product.find({}).limit(perPage).skip(page * perPage - perPage).populate("category", "name").exec(function (err, products) {
          if (err) return next(err);
          res.json({
            success: true,
            products: products,
            count: count
          })
        })
      })
    }
  },

  getCategories: function (req, res, next) {
    Product.aggregate({
          $group: {
            "_id": '$category',
            total_products: {
              $sum: 1
            }
          }
        }, function (err, response) {
          if (err) return next(err);
          Category.populate(response, {path: '_id', select: 'name'}, function(err, populatedCategories) {
            var total = 0,
                categories = [];
            populatedCategories.forEach(function(value){
              var category = {};
              total += value.total_products;
              category.total_products = value.total_products;
              if ( value._id ) category.name = value._id.name;
              categories.push(category);
            });
            res.json({
              success: true,
              total: total,
              categories: categories
            })
          });
        }
    );
  },

  getOne: function (req, res, next) {
    var id = req.params.id;
    var populateQuery = [{path: 'comments.user', select: 'avatar'}, {path: 'category', select: 'name'}];
    Product.findOne({_id: id}).lean().populate(populateQuery).exec(function (err, product) {
      if (err) return next(err);
      if (product) {
        res.json({
          success: true,
          product: product
        });
      } else {
        return res.status(404).json({
          success: false,
          message: 'Product with id ' + id + ' can not be found'
        });
      }
    })
  },

  createProduct: function (req, res, next) {
    console.log(JSON.stringify(req.body, undefined, 2));
    Category.findOne({name: req.body.category || ""}, function (err, cat) {
      if (err) return next(err);
      if (cat) {
        req.body.category = cat._id;
      } else {
        delete req.body.category;
      }
      Product.create(req.body, function (err, product) {
        if (err) return next(err);
        res.status(201).json({
          success: true,
          product: product
        });
      })
    })
  },

  update: function (req, res, next) {
    var id = req.params.id,
        body = {
          name: req.body.name,
          description: req.body.description,
          price: req.body.price,
          isInStock: req.body.isInStock
        };
    body.updatedAt = Date.now();
    Category.findOne({name: req.body.category}, function (err, cat) {
      if (err) return next(err);
      if (cat) {
        body.category = cat._id;
      } else {
        if ( body.name || body.description || body.price || body.isInStock) {
          body.category = undefined
        }
      }
      Product.findByIdAndUpdate(id, {$set: body, $inc: {__v: 1}}, {
        runValidators: true,
        new: true
      }, function (err, product) {
        if (err) return next(err);
        if (product) {

          res.json({
            success: true,
            product: product
          });
        } else {
          return res.status(404).json({
            success: false,
            message: 'Product with id ' + id + ' can not be found'
          });
        }
      });
    });
  },

  toggleStock: function (req, res, next) {
    var array = req.body.array,
        isInStock = req.body.isInStock;
    if (Array.isArray(array) && typeof isInStock == 'boolean') {
      Product.update({'_id': {$in: array}}, {$set: {isInStock: isInStock}}, {multi: true}, function (err) {
        if (err) return next(err);
        return res.status(201).json({
          success: true
        });
      })
    }
  },

  uploadPhotos: function (req, res, next) {
    var id = req.params.id,
        thumb = req.file;

    console.log(id);
    if (thumb) {
      console.log(thumb.filename);

      Product.findByIdAndUpdate(id, {$push: {images: thumb.filename}, $inc: {__v: 1}}, {
        runValidators: true,
        new: true
      }, function (err, product) {
        if (err) return next(err);
        if (product) {

          res.json({
            success: true,
            product: product,
            message: 'Photo for product was uploaded successfully, filename: ' + thumb.filename
          });
        } else {
          return res.status(404).json({
            success: false,
            message: 'Product with id ' + id + ' can not be found'
          });
        }
      });
    } else {
      res.status(500).json({
        success: false,
        message: 'Photo for product was NOT uploaded successfully'
      });
    }
  },

  deletePhoto: function (req, res, next) {
    var id = req.params.id,
        filename = req.params.filename;

    Product.findByIdAndUpdate(id, {$pull: {images: filename}, $inc: {__v: 1}}, {
      runValidators: true,
      new: true
    }, function (err, product) {
      if (err) return next(err);
      if (product) {
        fs.exists('./public/images/products/' + filename, function (exists) {
          if (exists) {
            fs.unlink('./public/images/products/' + filename);
            res.status(204).json({
              success: true,
              product: product,
              message: 'Photo for product was deleted successfully'
            });
          } else {
            res.status(403).json({
              success: false,
              message: 'Photo for product was NOT deleted successfully'
            });
          }
        });
      } else {
        return res.status(404).json({
          success: false,
          message: 'Product with id ' + id + ' can not be found'
        });
      }
    });
  },

  createComment: function (req, res, next) {
    var id = req.params.id,
        body = req.body,
        userId = req.user_id,
        comment = {
          user: userId,
          text: body.text
        };
    Product.findOneAndUpdate({_id: id}, {$push: {comments: comment}}, {new: true}).lean().populate('comments.user', 'avatar').exec(function (err, product) {
      if (err) return next(err);
      res.json({
        success: true,
        product: product
      })
    })
  },

  updateComment: function (req, res, next) {
    var id = req.params.id,
        comment_id = req.params.comment_id,
        body = req.body,
        userId = req.user_id;

    Product.findOneAndUpdate({
      _id: id,
      "comments._id": comment_id
    }, {$set: {"comments.$.text": body.text}}, {new: true}).lean().populate('comments.user', 'avatar').exec(function (err, product) {
      if (err) return next(err);
      if (product) {
        for (var i in product.comments) {
          if (product.comments[i]._id == comment_id) {
            var comment = product.comments[i];
          }
        }
        if (String(comment.user._id) == String(userId)) {
          if ((Date.now() - comment.createdAt.getTime()) < 120000) {
            //comment.text = body.text;
            res.json({
              success: true,
              product: product
            });
          } else {
            return res.status(404).json({
              success: false,
              message: 'It is to late to update this comment'
            });
          }
        } else {
          return res.status(403).json({
            success: false,
            message: 'You have no permission to update this comment'
          });
        }
      } else {
        return res.status(404).json({
          success: false,
          message: 'Product with id ' + id + ' can not be found'
        });
      }
    })
  },

  deleteComment: function (req, res, next) {
    var id = req.params.id,
        comment_id = req.params.comment_id;

    Product.findOneAndUpdate({_id: id}, {$pull: {comments: {_id: comment_id}}}, {new: true}).lean().populate('comments.user', 'avatar').exec(function (err, product) {
      if (err) return next(err);
      if (product) {
        res.json({
          success: true,
          product: product
        });
      } else {
        return res.status(404).json({
          success: false,
          message: 'Product with id ' + id + ' can not be found'
        });
      }
    })
  },

  search: function (req, res, next) {
    var searchQuery = (req.body && req.body.q) || (req.query && req.query.q);
    if (searchQuery && searchQuery.length > 2) {

      console.log(searchQuery.length);

      var userInput = new RegExp(searchQuery, 'i');

      Product.find(
          {
            //productsFound contains all the products that has user input in at least one field: name or description
            $or: [
              {name: {$regex: userInput}},
              {description: {$regex: userInput}}
            ]
          },
          {
            name: 1,
            description: 1,
            price: 1,
            _id: 1
          },
          function (err, productsFound) {
            if (err) return next(err);
            if (productsFound.length > 0) {
              return res.json({
                success: true,
                message: 'I found products!',
                products: productsFound
              });
            } else {
              return res.json({
                success: false,
                message: 'Sorry, I found no products!'
              });
            }
          })
    } else {
      return res.status(500).json({
        success: false,
        message: 'Please type more symbols'
      });
    }
  },

  getReviewed: function (req, res, next) {
    Product.find({_id: {$in: req.body.reviewed}},
        function (err, products) {
          if (err) return next(err);
          res.json({
            success: true,
            products: products
          })
        });
  },

  addtoWishlist: function (req, res, next) {
    var id = req.body.idProd,
        userId = req.body.userId;

    console.log('req.body.idProd');
    console.log(req.body.idProd);
    console.log('req.body.userId');
    console.log(req.body.userId);

    User.findOne({_id: userId}, function (err, user) {
      if (err) return next(err);
      if (user) {

        if (user.wishlist.indexOf(id) + 1) {
          res.json({
            success: false,
            message: 'product didn\'t added to wishlist'
          });
        } else {
          user.wishlist.push(id);
          user.save(function (err) {
            if (err) return next(err);

            res.json({
              success: true,
              message: 'add product to wishlist'
            });
          });
        }

      } else {
        return res.status(404).json({
          success: false,
          message: 'User with id ' + id + ' can not be found'
        });
      }
      console.log(user);
    })
  }
};

module.exports = products;
