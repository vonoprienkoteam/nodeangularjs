var express = require('express'),
    User = require('../models/user'),
    Product = require('../models/product'),
    settings = require('../config'),
    generateHash = settings.generateHash;
//sendEmail = settings.sendEmail;

var mailgun = require('mailgun-js')({
  apiKey: settings.config.getEnv().mailgun.key,
  domain: settings.config.getEnv().mailgun.domain
});

const fs = require('fs');

var users = {

  getAll: function (req, res, next) {
    User.find().select("-password").exec(function (err, users) {
      if (err) return next(err);
      res.json({
        success: true,
        users: users
      });
    })
  },

  getOne: function (req, res, next) {
    var id = req.params.id;
    User.findOne({_id: id}).select("-password").populate('orders').exec(function (err, user) {
      if (err) return next(err);
      if (user) {
        res.json({
          success: true,
          user: user
        });
      } else {
        return res.status(404).json({
          success: false,
          message: 'User with id ' + id + ' can not be found'
        });
      }
    })
  },

  update: function (req, res, next) {
    var id = req.params.id,
        body = {},
        avatar;

    //TODO: correcting email - save to database work correctly
    if (req.body.email) body.email = req.body.email;
    if (req.body.newPassword) body.newPassword = req.body.newPassword;
    if (req.body.admin) body.admin = req.body.admin;
    if (req.body.gender) body.gender = req.body.gender;
    if (req.body.gender) body.gender = req.body.gender;
    if (req.body.lastName) body.lastName = req.body.lastName;
    if (req.body.firstName) body.firstName = req.body.firstName;
    if (req.body.phone) body.phone = req.body.phone;
    body.banned = req.body.banned;
    if (req.file) {
      avatar = req.file.filename;
    }
    body.updatedAt = Date.now();

    User.findByIdAndUpdate(id, {$set: body, $inc: {__v: 1}}, {
      runValidators: true,
      new: true
    }, function (err, user) {
      if (err) return next(err);
      if (user) {
        // Replace old user's avatar
        if (avatar) {
          try {
            fs.unlinkSync('./public/uploads/' + user.avatar);
            user.avatar = avatar;
          }
          catch (ex) {
            user.avatar = avatar;
          }
        }

        user.save(function (err, user) {
          res.json({
            success: true,
            user: {
              //name: user.name,
              email: user.email,
              avatar: user.avatar,
              gender: user.gender,
              first_name: user.first_name,
              last_name: user.last_name,
              admin: user.admin,
              orders: user.orders,
              phone: user.phone
            }
          });
        });

      } else {
        return res.status(404).json({
          success: false,
          message: 'User with id ' + id + ' can not be found'
        });
      }
    });
  },

  createUser: function (req, res, next) {
    var body = {};

    if (req.body.name) body.name = req.body.name;
    if (req.body.admin) body.admin = req.body.admin;
    if (req.body.password) body.password = req.body.password;
    if (req.body.gender == "male") body.gender = "male";
    if (req.body.gender == "female") body.gender = "female";
    if (req.body.lastName) body.lastName = req.body.lastName;
    if (req.body.firstName) body.firstName = req.body.firstName;
    if (req.body.banned == 'true') body.banned = true;

    console.log(JSON.stringify(req.body));
    console.log(JSON.stringify(body));
    if (body.name && req.body.password == req.body.passwordConfirmation) {
      User.create(body, function (err, user) {
        if (err) return next(err);
        res.status(201).json({
          success: true,
          user: user
        })
      })
    }
  },

  updateYourself: function (req, res, next) {
    req.params.id = req.user_id;
    if (req.body.newPassword === req.body.confirmNewPassword) {
      req.body.admin = false;
      users.update(req, res, next);
    } else {
      return res.status(401).json({
        success: false,
        message: 'Password is not match with password confirmation.'
      });
    }
  },

  getYourself: function (req, res, next) {
    req.params.id = req.user_id;
    users.getOne(req, res, next);
  },

  getWishlist: function (req, res, next) {
    var id = req.params.id;

    User.findOne({_id: id}, function (err, user) {
      if (err) return next(err);

      if (user) {
        Product.find({_id: {$in: user.wishlist}},
            function (err, products) {
              if (err) return next(err);
              res.json({
                success: true,
                products: products
              })
            });
      } else {
        return res.status(404).json({
          success: false,
          message: 'User with id ' + id + ' can not be found'
        });
      }

    })
  },

  deleteWish: function (req, res, next) {
    var id = req.params.id,
        userId = req.params.userId;

    User.findOne({_id: userId}, function (err, user) {
      if (err) next(err);

      user.wishlist = user.wishlist.filter(function (productId) {
        return productId != id;
      });

      user.save(function (err, user) {
        if (err) next(err);

        Product.find({_id: {$in: user.wishlist}},
            function (err, products) {
              if (err) return next(err);
              res.json({
                success: true,
                products: products
              })
            });
      })
    });
  },

  restoreForgot: function (req, res, next) {
    var email = req.body.email;

    User
        .findOne({"email": email})
        .exec(function (err, user) {
          if (err) {
            return res.status(500).send({message: err});
          }
          if (user) {
            if (user.email) {
              user.passwordReset = Math.random().toString(36).slice(-8);
              user.save(function () {
                var content = 'Code for reseting your password: ' + user.passwordReset,
                    to = user.email,
                    title = 'Reset email';
                sendEmail(to, title, content);
                res.json({message: 'Email with additional information sent'});
              });
            } else {
              res.status(400).send({message: 'This account do not has email'});
            }
          } else {
            res.status(400).send({message: 'User not found'});
          }
        });
  },

  restoreReset: function (req, res) {
    var resetCode = req.body.code;

    User.findOne({passwordReset: resetCode}, function (err, user) {
      if (err) {
        return res.status(500).send({message: err});
      }

      if (user) {
        user.password = req.body.pass;
        console.log(req.body.pass);
        console.log(user.password);
        user.save(function (err, doc) {
          res.status(200).send({message: 'New password was successfully saved'});
        });
      } else {
        res.status(403).send({message: 'Wrong restore code!'});
      }
    });
  }
};

/**
 * Emails delivery function
 * @param to {string} - email of recipient
 * @param title {string} - email title
 * @param content {string} - email text or html
 */
function sendEmail(to, title, content) {
  if (to) {
    var data = {
      from: 'Arduino <noreply@arduino.com>',
      to: to,
      subject: title,
      text: content
    };

    mailgun.messages().send(data, function (error, body) {
      if (error) throw error;
      console.log(body);
    });
  }
};

module.exports = users;
