var jwt = require('jwt-simple'),
    User = require('../models/user'),
    config = require('../config');

var auth = {
    setup: function (req, res, next) {
        User.create({
            name: 'admin',
            password: 'password123',
            admin: true
        }, function (err, user) {
            if (err) return next(err);
            //res.send(genToken(user));
            console.log('Admin user saved successfully');
            res.json({
                success: true,
                message: 'Admin user saved successfully!'
            });
        })
    },

    login: function (req, res) {

        var name = req.body.name || '',
            password = req.body.password || '';

        if (name == '' || password == '') {
            res.status(401).json({
                success: false,
                message: "Invalid credentials"
            });
            return;
        }


        // Fire a query to your DB and check if the credentials are valid
        User.findOne({name: name}, function (err, user) {
            if (err) return next(err);
            if (user && !user.banned) {
                user.comparePassword(password, function (err, isMatch) {
                    if (err) return next(err);
                    console.log(isMatch);
                    if (isMatch) {
                        var dbUserObj = {
                            _id: user._id,
                            name: user.name,
                            admin: user.admin,
                            phone: user.phone || ""
                        };
                        res.send(genToken(dbUserObj));
                    } else {
                        res.status(401).json({
                            success: false,
                            message: "Invalid credentials"
                        });
                    }
                });
            } else {
                res.status(404).json({
                    success: false,
                    message: "Invalid credentials or you have no permission"
                });
            }
        });
    },

    register: function (req, res, next) {
        var name = req.body.name,
            password = req.body.password,
            password_confirmation = req.body.confirmpassword,
            usermail = req.body.usermail;

        if (name && password && password_confirmation && usermail) {
            if (password == password_confirmation) {
                User.create({
                    name: name,
                    password: password,
                    email: usermail
                }, function (err, user) {
                    if (err) return next(err);
                    //// Authenticate just created user
                    //auth.login(req, res);

                    res.send(genToken(user));
                    //res.json({
                    //    success: true,
                    //    user: user
                    //});
                })
            } else {
                return res.status(401).json({
                    success: false,
                    message: 'Password is not match with password confirmation.'
                });
            }
        } else {
            return res.status(500).json({
                success: false,
                message: 'Invalid credentials.'
            });
        }
    },

    checkmail: function (req, res, next) {
        var mail = req.body.email;
        User
            .findOne({ email: mail }, function (err, user) {
                if (err) next(err);
                console.log(user);
                res.send(user);
            });
    }
};

// private method
function genToken(user) {
    var expires = expiresIn(7); // 7 days
    var token = jwt.encode({
        exp: expires,
        user: user // add the user object to the token
    }, config.secret);

    return {
        success: true,
        token: token,
        expires: expires,
        user: {
            "_id": user._id,
            "name": user.name,
            "admin": user.admin,
            "phone": user.phone
        }
    };
}

function expiresIn(numDays) {
    var dateObj = new Date();
    return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;