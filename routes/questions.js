var express = require('express'),
    Product = require('../models/product');

var questions = {

    createQuestion: function(req, res, next) {
        var id = req.params.id,
            body = req.body,
            userId = req.user_id;

        Product.findOne({_id: id}, function(err, question) {
            if (err) return next(err);

            if (question) {
                question.support.push({
                    user_id: userId,
                    question: body.question,
                    product_id: body.productId
                });
                question.save(function(err) {
                    if (err) return next(err);
                    res.json({
                        success: true,
                        question: question
                    })
                });
            } else {
                return res.status(404).json({
                    success: false,
                    message: 'Question with id ' + id + ' can not be found'
                });
            }
        })
    },

    getAll: function(req, res, next) {

        Product.find(function(err, products){
            if (err) return next(err);
            res.json({
                success: true,
                products: products
            })
        })
    },

    getOne: function(req, res, next) {
        var idProduct = req.params.idProduct;
            //idQuestion = req.params.idQuestion;


        Product.findOne( { _id : idProduct} )
                .populate('support.user_id', '_id name avatar')
                .exec(function(err, product){
                if (err) return next(err);
                res.json({
                    success: true,
                    product: product.support
                })
        })
    },

    createAnswer: function (req, res, next) {
        var idProduct = req.params.idProduct,
            newAnswer = req.body.newAnswer,
            isAdmin = req.body.isAdmin || false;
            idQuestion = req.body.idQuestion;

        Product.findOne({_id: idProduct}, function(err, product) {
            if (err) return next(err);
            if (product) {
            product.support.forEach(function (sup) {
                if (sup._id == idQuestion) {
                    return sup.answers.push({
                            answer: newAnswer,
                            isAdmin: isAdmin
                    })
                }
            });
                product.save(function(err) {
                    if (err) return next(err);
                    res.json({
                        success: true,
                        answer: product
                    })
                });

            } else {
                return res.status(404).json({
                    success: false,
                    message: 'Question with id ' + id + ' can not be found'
                });
            }
        })
    },

    getUserQuestions : function (req, res, next) {
        var userId = req.params.userId;

        Product.find(
            { 'support.user_id' : userId },
            function (err, products) {
                if (err) return next(err);

            res.json({
                success: true,
                products: products
            });
            }
        )
    },

    getForUserOne : function (req, res, next) {
        var idProduct = req.params.idProduct;

        Product.findOne({_id : idProduct}, function(err, product){
            if (err) return next(err);
            res.json({
                success: true,
                product: product.support
            })
        })
    }

};

module.exports = questions;
