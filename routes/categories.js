var express = require('express'),
    Category = require('../models/category');
    //mongoose = require('mongoose');

var categories = {

  getAll: function (req, res, next) {
    Category.count({}, function( err, count) {
      if (err) return next(err);
      Category.find({}).exec(function (err, categories) {
        if (err) return next(err);
        res.json({
          success: true,
          categories: categories,
          count: count
        })
      });
    })
  },

  getOne: function (req, res, next) {
    var id = req.params.id;
    Category.findOne({_id: id}, function (err, category) {
      if (err) return next(err);
      if (category) {
        res.json({
          success: true,
          category: category
        });
      } else {
        return res.status(404).json({
          success: false,
          message: 'Category with id ' + id + ' can not be found'
        });
      }
    })
  },

  create: function (req, res, next) {
    Category.create(req.body, function (err, category) {
      if (err) return next(err);
      res.status(201).json({
        success: true,
        category: category
      });
    })
  },

  update: function (req, res, next) {
    var id = req.params.id,
        body = {
          name: req.body.name
        };
    body.updatedAt = Date.now();

    if (body.name) {
      Category.findByIdAndUpdate(id, {$set: body, $inc: {__v: 1}}, {
        runValidators: true,
        new: true
      }, function (err, category) {
        if (err) return next(err);
        if (category) {
          res.json({
            success: true,
            category: category
          });
        } else {
          return res.status(404).json({
            success: false,
            message: 'Category with id ' + id + ' can not be found'
          });
        }
      });
    }
  },

  delete: function (req, res, next) {
    var id = req.params.id;

    Category.findOne({_id: id}, function (err, category) {
      if (err) return next(err);
      if (category) {
        category.remove(function (err) {
          if (err) return next(err);
          return res.json({
            success: true,
            message: 'Category was deleted successfully!'
          });
        })
      } else {
        return res.status(404).json({
          success: false,
          message: 'Category with id ' + id + ' can not be found'
        });
      }
    })
  }

};

module.exports = categories;
