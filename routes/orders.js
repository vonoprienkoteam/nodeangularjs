var express = require('express'),
    User = require('../models/user'),
    Order = require('../models/order');

var orders = {
    checkout: function (req, res, next) {
        var user = req.body.user,
            order = {
                paid: false,
                totalCost: req.body.data.totalCost,
                products: req.body.data.items,
                address: user.address,
                phone: user.phone,
                message: user.message
            };

        if (user.id) {
            User.findOne({_id: user.id}, function (err, user) {
                if (err) return next(err);
                if (user) {
                    order.user = user._id;
                    Order.create(order, function (err, order) {
                        if (err) return next(err);
                        user.orders.push(order);
                        user.save(function (err) {
                            if (err) return next(err);
                        });
                        res.json({
                            success: true,
                            order: order
                        })
                    });
                }
            });
        } else {
            order.guest = user.guest;
            Order.create(order, function (err, order) {
                if (err) return next(err);
                res.json({
                    success: true,
                    order: order
                })
            });
        }
    },

    getAll: function (req, res, next) {
        Order.find().populate("user", "name avatar").exec(function (err, orders) {
            if (err) return next(err);
            res.json({
                success: true,
                orders: orders
            });
        })
    },

    getOne: function(req, res, next) {
        var id = req.params.id;
        Order.findOne({_id: id}).populate("user", "name avatar").exec(function(err, order) {
            if (err) return next(err);
            if (order) {
                res.json({
                    success: true,
                    order: order
                });
            } else {
                return res.status(404).json({
                    success: false,
                    message: 'Order with id ' + id + ' can not be found'
                });
            }
        })
    },

    update: function(req, res, next) {
        var id = req.params.id,
            body = {
                paid: req.body.paid,
                updatedAt: Date.now()
            };

        Order.findByIdAndUpdate(id, {$set: body, $inc: {__v: 1}}, {
            runValidators: true,
            new: true
        }).populate("user", "name avatar").exec(function(err, order) {
            if (err) return next(err);
            if (order) {
                res.json({
                    success: true,
                    order: order
                });
            } else {
                return res.status(404).json({
                    success: false,
                    message: 'Order with id ' + id + ' can not be found'
                });
            }
        });
    }
};

module.exports = orders;
