var express = require('express'),
    Order = require('../models/order'),
    Category = require('../models/category'),
    Product = require('../models/product'),
    User = require('../models/user');

var reports = {

  getOrdersStats: function (req, res, next) {

    Order.count({paid: true}, function( err, count) {
      if (err) return next(err);
      Order.find({paid: false}).exec(function (err, orders) {
        if (err) return next(err);
        res.json({
          success: true,
          stats: {"cols": [
            {id: "t", label: "Status", type: "string"},
            {id: "s", label: "Quantity", type: "number"}
          ], "rows": [
            {c: [
              {v: "Paid orders"},
              {v: count}
            ]},
            {c: [
              {v: "Not paid orders"},
              {v: orders.length}
            ]}
          ]}
        })
      });
    })

  },

  getCategoriesStats: function (req, res, next) {

    var stats = {
          "cols": [
            {id: "t", label: "Category", type: "string"},
            {id: "s", label: "Products", type: "number"}
          ], "rows": []
        };

    Product.aggregate({
        $group: {
          "_id": '$category',
          total_products: {
            $sum: 1
          }
        }
      }, function (err, response) {
        if (err) return next(err);
          Category.populate(response, {path: '_id', select: 'name'}, function(err, populatedCategories) {
            populatedCategories.forEach(function(value){
              stats.rows.push(
                {c: [
                  {v: value._id && value._id.name || "no category"},
                  {v: value.total_products}
                ]}
              )
            });
            res.json({
              success: true,
              stats: stats
            })
          });
      }
    );

  },

  getProductsStats: function (req, res, next) {

    var stats = {
      "cols": [
        {id: "t", label: "Product", type: "string"},
        {id: "s", label: "quantity", type: "number"}
      ], "rows": []
    };

    Order.aggregate({
          $unwind: "$products"
        }, {
          $project: {
            _id: 0,
            p_id: "$products.id",
            count: {
              $add: [1]
            }
          }
        }, {
          $group: {
            _id: "$p_id",
            number: {
              $sum: "$count"
            }
          }
        }, {
          $sort: {
            number: -1
          }
        },{
          $limit: 10
        }, function (err, response) {
          if (err) return next(err);
          Product.populate(response, {path: '_id', select: 'name'}, function(err, populatedProducts) {
            populatedProducts.forEach(function(value){
              if (value._id) {
                stats.rows.push(
                    {
                      c: [
                        {v: value._id.name},
                        {v: value.number}
                      ]
                    }
                )
              }
            });
            res.json({
              success: true,
              stats: stats
            })
          });
        }
    );

  },

  getUsersStats: function (req, res, next) {
    User.count({}, function( err, count) {
      if (err) return next(err);
      User.find({}).exec(function (err, users) {
        if (err) return next(err);
        res.json({
          success: true,
          users: users,
          count: count
        })
      });
    })
  },

  getSalesStats: function (req, res, next) {
    var stats = {
          "cols": [
            {id: "month", label: "Month", type: "date"},
            {id: "paid-data", label: "Total orders", type: "number"},
            {id: "not-paid-data", label: "Not paid orders", type: "number"}
          ],
          "rows": []
        },
        row = {c: []},
        total_orders = 0,
        total_paid = 0,
        total_not_paid = 0;

    Order.aggregate({
          $project: {
            year: { $year: "$createdAt" },
            month: { $month: "$createdAt" },
            day: { $dayOfMonth: "$createdAt" },
            hour: { $hour: "$createdAt" },
            paid: "$paid"
          }
        }, {
          $group: {
            _id: {
              "year": "$year",
              "month": "$month",
              "day": "$day",
              "hour": "$hour",
              "paid": "$paid"
            },
            count: {
              $sum : 1
            }
          }
        }, {
          $group: {
            _id: {
              "year": "$_id.year",
              "month": "$_id.month",
              "day": "$_id.day",
              "hour": "$_id.hour"
            },
            order_status: {
              $addToSet: {
                paid: "$_id.paid",
                count: "$count"
              }
            }
          }
        }, {
          $sort: {"_id.year": 1, "_id.month": 1, "_id.day": 1, "_id.hour": 1}
        }, function (err, response) {
          if (err) return next(err);

          response.forEach(function(value) {

            if (typeof value.order_status[1] != 'undefined') {
              total_paid += value.order_status[0].count;
              total_not_paid += value.order_status[1].count;
            } else {
              if (value.order_status[0].paid) {
                total_paid += value.order_status[0].count;
              } else {
                total_not_paid += value.order_status[0].count;
              }
            }
            total_orders = total_paid + total_not_paid;

            row.c.push({v: new Date(value._id.year, value._id.month-1, value._id.day, value._id.hour)});
            row.c.push({v: total_orders});
            row.c.push({v: total_not_paid});

            stats.rows.push(row);
            row = {c: []};

          });

          res.json({
            success: true,
            stats: stats
          })
        }
    );
  }

};

module.exports = reports;
