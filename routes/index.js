var express = require('express'),
    router = express.Router(),
    validateRequest = require('../middlewares/validateRequest'),
    User = require('../models/user'),
    auth = require('./auth.js'),
    products = require('./products.js'),
    orders = require('./orders.js'),
    user = require('./users.js'),
    questions = require('./questions.js'),
    categories = require('./categories.js'),
    reports = require('./reports.js'),
    multer = require('multer'),
    uploadAvatar = multer({dest: './public/uploads/'}),
    storage = multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null, './public/images/products/')
      },
      filename: function (req, file, cb) {
        cb(null, req.params.id + '-' + Date.now())
      }
    }),
    uploadThumbs = multer({storage: storage});

/*
 * Routes that can be accessed by any one
 */
router.post('/login', auth.login);
router.get('/products', products.getAll);
router.get('/products/categories', products.getCategories);
router.get('/product/:id', products.getOne);
router.post('/register', auth.register);
router.get('/setup', auth.setup);
router.get('/products/search', products.search);
router.post('/checkout', orders.checkout);
router.post('/user/restore/forgot', user.restoreForgot);
router.post('/user/restore/reset', user.restoreReset);
router.post('/checkmail', auth.checkmail);


router.post('/products/reviewed', products.getReviewed);
/*
 * Routes that can be accessed only by autheticated users
 */
router.use(validateRequest);
router.post('/product/:id/comment', products.createComment);
router.put('/product/:id/comment/:comment_id', products.updateComment);
router.get('/user', user.getYourself);
router.post('/user/avatar', uploadAvatar.single('file'), user.updateYourself);
router.put('/user', user.updateYourself);


// questions and answers for products

router.post('/product/:id/question', questions.createQuestion);
router.post('/user/products/:idProduct/answer', questions.createAnswer);
router.get('/products/questions', questions.getAll);
router.get('/user/products/:idProduct/question/:idQuestion', questions.getForUserOne);
router.get('/user/:userId/questions', questions.getUserQuestions);


// wishlist
router.post('/products/addtowishlist', products.addtoWishlist);
router.get('/products/wishlist/:id', user.getWishlist);
router.delete('/products/wishlist/:userId/:id', user.deleteWish);

/*
 * Routes that can be accessed only by authenticated & authorized users
 */

// question

router.get('/admin/products/:idProduct/question/:idQuestion', questions.getOne);
router.post('/admin/products/:idProduct/answer', questions.createAnswer);


//products

router.get('/admin/products', products.getAll);
router.post('/admin/product/', products.createProduct);
router.put('/admin/product/:id', products.update);
router.put('/admin/products', products.toggleStock);
router.post('/admin/product/:id/thumbs', uploadThumbs.single('file'), products.uploadPhotos);
router.delete('/admin/product/:id/thumb/:filename', products.deletePhoto);


// comments

router.delete('/admin/product/:id/comment/:comment_id', products.deleteComment);


// users

router.get('/admin/users', user.getAll);
router.post('/admin/users', user.createUser);
router.get('/admin/user/:id', user.getOne);
router.put('/admin/user/:id', user.update);


// orders

router.get('/admin/orders', orders.getAll);
router.get('/admin/order/:id', orders.getOne);
router.put('/admin/order/:id', orders.update);


// categories

router.get('/admin/categories', categories.getAll);
router.get('/admin/categories/:id', categories.getOne);
router.post('/admin/categories', categories.create);
router.put('/admin/categories/:id', categories.update);
router.delete('/admin/categories/:id', categories.delete);


// reports

router.get('/admin/reports/orders', reports.getOrdersStats);
router.get('/admin/reports/categories', reports.getCategoriesStats);
router.get('/admin/reports/products', reports.getProductsStats);
//router.get('/admin/reports/users', reports.getUsersStats);
router.get('/admin/reports/sales', reports.getSalesStats);

module.exports = router;
