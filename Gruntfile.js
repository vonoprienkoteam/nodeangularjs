module.exports = function(grunt) {
    grunt.initConfig({
        bowercopy: {
            options: {
                    srcPrefix: 'bower_components'
                },

                libs: {
                    options: {
                        destPrefix: 'public/libs'
                    },

                    files: {
                        // Angular JS
                        'angular.min.js': 'angular/angular.min.js',
                        'angular.min.js.map': 'angular/angular.min.js.map',

                        // Angular Route
                        'angular-route.min.js': 'angular-route/angular-route.min.js',
                        'angular-route.min.js.map': 'angular-route/angular-route.min.js.map',

                        // Angular Resource
                        'angular-resource.min.js': 'angular-resource/angular-resource.min.js',
                        'angular-resource.min.js.map': 'angular-resource/angular-resource.min.js.map',

                        // Angular Pagination
                        'dirPagination.js': 'angularUtils-pagination/dirPagination.js',
                        'dirPagination.tpl.html': 'angularUtils-pagination/dirPagination.tpl.html',

                        // bootstrap Resource
                        'bootstrap.min.css': 'bootstrap/dist/css/bootstrap.min.css',
                        'bootstrap.min.css.map': 'bootstrap/dist/css/bootstrap.min.css.map',

                        'bootstrap-theme.min.css': 'bootstrap/dist/css/bootstrap-theme.min.css',
                        'bootstrap-theme.min.css.map': 'bootstrap/dist/css/bootstrap-theme.min.css.map',

                        'bootstrap.min.js': 'bootstrap/dist/js/bootstrap.min.js',

                        // Angular formly
                        'formly.min.js': 'angular-formly/dist/formly.min.js',
                        'api-check.min.js': 'api-check/dist/api-check.min.js',
                        'angular-formly-templates-bootstrap.min.js': 'angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap.min.js',

                        // Angular file uploader
                        'angular-file-upload.min.js': 'angular-file-upload/dist/angular-file-upload.min.js',

                        // Bootstrap toggle
                        'bootstrap-toggle.min.js': 'bootstrap-toggle/js/bootstrap-toggle.min.js',
                        'bootstrap-toggle.min.css': 'bootstrap-toggle/css/bootstrap-toggle.min.css',

                        // Angular Google charts
                        'ng-google-chart.min.js': 'angular-google-chart/ng-google-chart.min.js',

                        // Fuzzy filter
                        'angular-filter.min.js':'angular-filter/dist/angular-filter.min.js',

                        // jquery Resource
                        'jquery.min.js': 'jquery/dist/jquery.min.js',
                        'jquery.min.js.map': 'jquery/dist/jquery.min.map'
                    }
                }
        },
        jade: {
            compile: {
                options: {
                    client: false,
                    pretty: true
                },
                files: [ {
                    cwd: "public/js/template/jadeFile",
                    src: "**/*.jade",
                    dest: "public/js/template",
                    expand: true,
                    ext: ".html"
                } ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.loadNpmTasks("grunt-contrib-jade");

    grunt.registerTask('copy', [
        'bowercopy'
    ]);

    grunt.registerTask('jade2html', [
        'jade'
    ]);
};
